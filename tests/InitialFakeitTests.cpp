TEST_CASE("Initial Fakeit Test") {

    struct SomeInterface {
        virtual int foo(int) = 0;
        virtual int bar(std::string) = 0;
    };

    fakeit::Mock<SomeInterface> mockSimple; // Create Mock
    fakeit::When(Method(mockSimple,foo)).Return(42); // Method mock.foo will return 42 once.

    fakeit::Mock<AccessHelper> mockComplex;
    fakeit::When(Method(mockComplex, accessHelperTest_getTestVariable)).Return(21);

    SECTION("Should return the mocked value of method foo of mockSimple") {
        // Fetch the mock instance.
        SomeInterface &i = mockSimple.get();
        REQUIRE(i.foo(0) == 42);
    }

    SECTION("Should return the mocked value of method accessHelperTest_getTestVariable of mockComplex") {
        AccessHelper &i = mockComplex.get();
        REQUIRE(i.accessHelperTest_getTestVariable() == 21);
    }
}