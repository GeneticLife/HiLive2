#include <cmath>
TEST_CASE("Initial Test") {
    SECTION("Test AccessHelper class's ability to access private functions and member variables") {
        AccessHelper helper;
        int value = 1;
        helper.accessHelperTest_setTestVariable(value);
        REQUIRE(helper.accessHelperTest_getTestVariable() == value);
    }

    SECTION("test mathematical constraints") {
        REQUIRE(-cos(M_PI) == 1);
    }
}

// Compile & run:
// - g++ -std=c++11 -Wal -o 010-TestCase 010-TestCase.cpp && ./010-TestCase --success