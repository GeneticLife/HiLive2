#include <string>
#include "../lib/kindex.h"
#include <iostream>
#include <fstream>


TEST_CASE("KixBuild Test") {

    KixBuild kixBuild;

    std::string mockFilename = "../tests/mock_objects/mock_fasta.fa";
    std::string indexFolder = "../tests/mock_objects/index/";
    std::string indexFile = indexFolder + "hiv";

    // setup
    namespace fs=boost::filesystem;
    fs::create_directory(indexFolder);

    /*
     * The header of the sequences that are indexed look like this:
     * >NC_001802.1 Human immunodeficiency virus 1, part1
     * >NC_001803.1 Human immunodeficiency virus 1, part2
     *
     * The first symbol (">") marks this as a header, the rest is potentially the name.
     * We can set parameter if we want to trim the name to the first word or convert spaces into underscores.
     *
     * The actual index is created by seqan, but kixBuild.create_index saves the data of the sequences in its own datastructures
     * (seq_names, seq_lengths, seqs)
     */

    SECTION("Should be able to create an index for the DNA (do not trim ID)") {
        int returnCode = kixBuild.create_index(mockFilename, indexFile, false, false);
        // do not trim and do not convert spaces
        REQUIRE(returnCode == 0);
        REQUIRE(kixBuild.seq_names[0] == "NC_001802.1 Human immunodeficiency virus 1, part1");
        REQUIRE(kixBuild.seq_names[1] == "NC_001803.1 Human immunodeficiency virus 1, part2");
    };

    SECTION("Should be able to create an index for the DNA (trim ID)") {
        int returnCode = kixBuild.create_index(mockFilename, indexFile, false, true);
        // trim the name
        REQUIRE(returnCode == 0);
        REQUIRE(kixBuild.seq_names[0] == "NC_001802.1");
        REQUIRE(kixBuild.seq_names[1] == "NC_001803.1");
    };

    SECTION("Should be able to create an index for the DNA (convert spaces)") {
        int returnCode = kixBuild.create_index(mockFilename, indexFile, true, false);
        // convert spaces to underscores
        REQUIRE(returnCode == 0);
        REQUIRE(kixBuild.seq_names[0] == "NC_001802.1_Human_immunodeficiency_virus_1,_part1");
        REQUIRE(kixBuild.seq_names[1] == "NC_001803.1_Human_immunodeficiency_virus_1,_part2");
    };

    SECTION("Should be able to parse the correct data") {
        int returnCode = kixBuild.create_index(mockFilename, indexFile, false, false);
        REQUIRE(returnCode == 0);
        // get the first 5 bases of the sequence
        seqan::Prefix<seqan::DnaString >::Type prefixA(kixBuild.seqs[0], 5);
        seqan::Prefix<seqan::DnaString >::Type prefixB(kixBuild.seqs[1], 5);

        // the sequences of the index are not the input sequences but the matching basepairs
        REQUIRE(prefixA == "CCAGA");
        REQUIRE(prefixB == "AGTCA");
    };

    // clean up
    if(fs::exists(indexFolder))
        if(!fs::remove_all(indexFolder))
            std::cerr << "Error deleting directory: " + indexFolder<< std::endl;
}