//
// Created by leonard on 12.11.18.
//
TEST_CASE("FilterParserTests") {
    // Create Parser, File and example data for the File
    // Happens befor every SECTION
    FilterParser filterParser;
    std::ofstream outfile ("filterParserTest.filter");
    std::string mockFile = "0123456789abcd";
    std::string mockFile2 = "012";

    SECTION("Should open the filter file and return the right length of the file") {
        // bclParser.open() returns data.size()
        // data.size() is the number of character in the file
        outfile << mockFile << std::endl;
        uint64_t returnValue = filterParser.open("filterParserTest.filter");
        // The number of characters in our mock file is 15
        REQUIRE(returnValue == 15);
    }
    SECTION("Should return the number of reads inside the file") {
        outfile << mockFile << std::endl;
        filterParser.open("filterParserTest.filter");
        uint32_t num_reads = 0;
        // The number of reads are located at position 8 in our file and are 4 bytes long
        memcpy(&num_reads,mockFile.data()+8,4);
        // filterParser.size() returns the number of reads of our file
        REQUIRE(num_reads == filterParser.size());
    }
    SECTION("Should set the pointer to the next position in the file and if this is possible, return true (position < data.size())") {
        outfile << mockFile << std::endl;
        filterParser.open("filterParserTest.filter");
        // mockChar is a pointer to the first position of the actual data
        // In case of the filter files, the data starts at position 5
        char mockChar = *(mockFile.data() + 8);
        // The next() function gives us the pointer to the next element in the file
        // And return true, if another element exists
        REQUIRE((mockChar > 0) == filterParser.next());
    }
    SECTION("Should throw an error because position > data.size()") {
        outfile << mockFile2 << std::endl;
        filterParser.open("filterParserTest.filter");
        // Because position > data.size(), the next() function return a runtime error
        REQUIRE_THROWS_AS(filterParser.next(), std::runtime_error);
    }
    SECTION("Should return true, if the next read exists") {
        outfile << mockFile << std::endl;
        filterParser.open("filterParserTest.filter");
        bool hasNext = filterParser.has_next();
        REQUIRE(hasNext);
    }
    SECTION("Should return false, if the next read does not exists") {
        outfile << mockFile2 << std::endl;
        filterParser.open("filterParserTest.filter");
        bool hasNext = filterParser.has_next();
        REQUIRE(!hasNext);
    }
    // remove the file after every SECTION
    remove ("filterParserTest.filter");
}
