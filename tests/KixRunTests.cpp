#include <string>
#include "../lib/kindex.h"
#include <iostream>
#include <fstream>
#include<stdio.h>

TEST_CASE("KixRun Test") {
    KixRun kixRun;

    std::string indexFolder = "../tests/mock_objects/index_mock/";
    // the mocked files are an index for two sequences of HIV
    std::string indexFile = indexFolder + "hiv_mock";

    SECTION("Should be able to load the seqnames") {
        // load_seqnames returns an bool even though the signature says it returns an int
        bool success = kixRun.load_seqnames(indexFile);
        REQUIRE(success);
        // these are the two names of the sequences that were indexed
        REQUIRE(kixRun.getSeqNames()[0] == "NC_001802.1 Human immunodeficiency virus 1, part1");
        REQUIRE(kixRun.getSeqNames()[1] == "NC_001803.1 Human immunodeficiency virus 1, part2");
    }

    SECTION("Should be able to load the seqlengths") {
        // load_seqlengths returns an bool even though the signature says it returns an int
        bool success = kixRun.load_seqlengths(indexFile);
        REQUIRE(success);
        // each sequence consists of 350 nucleotid bases
        REQUIRE(kixRun.getSeqLengths()[0] == 350);
        REQUIRE(kixRun.getSeqLengths()[1] == 350);
    }

    SECTION("Should be able to load the metadata") {
        // load_metadata is the combination of load_seqnames and load_seqlengths
        // load_metadata returns an bool even though the signature says it returns an int
        bool success = kixRun.load_metadata(indexFile);
        REQUIRE(success);
        REQUIRE(kixRun.getSeqLengths()[0] == 350);
        REQUIRE(kixRun.getSeqLengths()[1] == 350);
        REQUIRE(kixRun.getSeqNames()[0] == "NC_001802.1 Human immunodeficiency virus 1, part1");
        REQUIRE(kixRun.getSeqNames()[1] == "NC_001803.1 Human immunodeficiency virus 1, part2");
    }

    SECTION("Should be able to load the index") {
        // we only test the status code because the method only calls an method of seqan and we trust that it behaves correctly if the return value is correct
        int returnStatus = kixRun.load_fmindex(indexFile);
        REQUIRE(returnStatus == 0);
    }
}