TEST_CASE("hiliveTest") {
  SECTION("Should execute main function successfully") {
    int mockArgc = 11;
    const char* mockArgv[] = {
        "./hilive", "-b", "../tutorial/BaseCalls", "-i", "../tutorial/index/", "--lanes", "1", "--tiles", "1101", "-r", "10R"
    };

    int returnCode = testableMain(mockArgc, mockArgv);
    REQUIRE ( returnCode == EXIT_SUCCESS);
  }
}

// Compile & run:
// - g++ -std=c++11 -Wal -o hilive hilive.cpp; ./hilive --success