class AccessHelperTest {
private:
    friend class AccessHelper;

    int testVariable;

    void setTestVariable(int value) {
        testVariable = value;
    }

    int getTestVariable() {
        return testVariable;
    }

};

class AccessHelper {
    AccessHelperTest accessHelperTest;

public:

    TEST_VIRTUAL void accessHelperTest_setTestVariable(int value) {
        accessHelperTest.setTestVariable(value);
    }

    TEST_VIRTUAL int accessHelperTest_getTestVariable() {
        return accessHelperTest.getTestVariable();
    };
};