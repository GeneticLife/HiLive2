TEST_CASE("BuildIndexArgumentParserTests") {
  char mockInputFile[] = "input_attr";
  FILE *fp = fopen(mockInputFile, "ab+");

  const char *defaultMockArgv[] = {
      "./hilive-build", "-i", mockInputFile, "-o", "output_file_prefix_attr"
  };

  // Capture cout
  std::streambuf *defaultOutBuffer = std::cout.rdbuf();
  std::ostringstream capturedOutBuffer;
  std::cout.rdbuf(capturedOutBuffer.rdbuf());


  SECTION("Should interpret positional options correctly") {
    // Test short parameter (-i, -o)

    BuildIndexArgumentParser argumentParser(5, defaultMockArgv);
    argumentParser.parseCommandLineArguments();

    REQUIRE(argumentParser.fasta_name == "input_attr");
    REQUIRE(argumentParser.index_name == "output_file_prefix_attr");

    REQUIRE(argumentParser.fasta_name != "anything");
    REQUIRE(argumentParser.index_name != "inappropiate");


    // Test long parameter (--input, -out-prefix)
    const char *mockArgv[] = {
        "./hilive-build", "--input", mockInputFile, "--out-prefix", "other_file_prefix_attr"
    };

    argumentParser = BuildIndexArgumentParser(5, mockArgv),
        argumentParser.parseCommandLineArguments();

    REQUIRE(argumentParser.fasta_name == "input_attr");
    REQUIRE(argumentParser.index_name == "other_file_prefix_attr");
  }

  SECTION("Should interpret build options correctly") {
    // If not specified, build options are set to false
    BuildIndexArgumentParser argumentParser(5, defaultMockArgv);
    argumentParser.parseCommandLineArguments();

    REQUIRE(argumentParser.do_not_convert_spaces == false);
    REQUIRE(argumentParser.trim_ids == false);

    // If specified, build options are set to true
    const char *mockArgv[] = {
        "./hilive-build", "-i", mockInputFile, "-o", "output_file_prefix_attr",
        "--do-not-convert-spaces", "--trim-after-space"
    };

    argumentParser = BuildIndexArgumentParser(7, mockArgv);
    argumentParser.parseCommandLineArguments();

    REQUIRE(argumentParser.do_not_convert_spaces == true);
    REQUIRE(argumentParser.trim_ids == true);
  }

  SECTION("Should print help correctly, if required") {
    const char *mockArgv[] = {"./hilive-build", "--help"};

    capturedOutBuffer.str(""); // clear buffer

    BuildIndexArgumentParser argumentParser(2, mockArgv);
    argumentParser.parseCommandLineArguments();

    REQUIRE(capturedOutBuffer.str() == argumentParser.help + '\n');

  }

  SECTION("Should print license correctly, if required") {
    const char *mockArgv[] = {"./hilive-build", "--license"};

    capturedOutBuffer.str(""); // clear buffer

    BuildIndexArgumentParser argumentParser(2, mockArgv);
    argumentParser.parseCommandLineArguments();

    REQUIRE(capturedOutBuffer.str() == argumentParser.license + '\n');
  }

  fclose(fp);
  remove(mockInputFile);

  // Reset cout
  std::cout.rdbuf(defaultOutBuffer);

}

// Compile & run:
// - g++ -std=c++11 -Wal -o hilive hilive.cpp; ./hilive --success