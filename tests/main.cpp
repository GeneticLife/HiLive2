// Let Catch provide main():
#define CATCH_CONFIG_MAIN

#include "catch2/catch.hpp"
#include "./fakeit/fakeit.hpp"
#include "../tools/hilive.h"
#include "AccessHelper.cpp"
#include "InitialTests.cpp"
#include "AdditionalInitialTests.cpp"
#include "HiliveTests.cpp"
#include "BuildIndexArgumentParserTests.cpp"
#include "BclParserTests.cpp"
#include "FilterParserTests.cpp"
#include "InitialFakeitTests.cpp"
#include "KixBuildTests.cpp"
#include "KixRunTests.cpp"
