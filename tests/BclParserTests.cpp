//
// Created by leonard on 12.11.18.
//
TEST_CASE("BclParserTests") {
    // Create parser, file and example data for the file
    // Happens befor every SECTION
    BclParser bclParser;
    std::ofstream outfile("bclParserTest.bcl");
    std::string mockFile = "0123456789";
    std::string mockFile2 = "012";

    SECTION("Should open the filter file and return the right length of the file") {
        outfile << mockFile << std::endl;
        // bclParser.open() returns data.size()
        // data.size() is the number of character in the file
        uint64_t returnValue = bclParser.open("bclParserTest.bcl");
        // The number of characters in our mock file is 11
        REQUIRE(returnValue == 11);
    }
    SECTION("Should return the number of reads inside the file") {
        outfile << mockFile << std::endl;
        bclParser.open("bclParserTest.bcl");
        uint32_t num_reads = 0;
        // The first 4 Bytes in every bcl-file are metadata (number of the reads)
        memcpy(&num_reads, mockFile.data(), 4);
        // bclParser.size() returns the number of reads
        REQUIRE(num_reads == bclParser.size());
    }
    SECTION("Should set the pointer to the next position in the file and return it (position < data.size())") {
        outfile << mockFile << std::endl;
        bclParser.open("bclParserTest.bcl");
        // mockChar is a pointer to the first position of the actual data
        // In case of the bcl files, the data starts at position 5
        char mockChar = *(mockFile.data() + 4);
        // The function next gives us the pointer to the next element in the file
        // And return a pointer to the next element
        REQUIRE(mockChar == bclParser.next());
    }
    SECTION("Should throw an error because position > data.size() and its not possible to get the next read)") {
        outfile << mockFile2 << std::endl;
        bclParser.open("bclParserTest.bcl");
        // Because position > data.size(), the next() function return a runtime error
        REQUIRE_THROWS_AS(bclParser.next(), std::runtime_error);
    }
    SECTION("Should return true, if the next read exists") {
        outfile << mockFile << std::endl;
        bclParser.open("bclParserTest.bcl");
        bool hasNext = bclParser.has_next();
        REQUIRE(hasNext);
    }
    SECTION("Should return false, if the next read does not exists") {
        outfile << mockFile2 << std::endl;
        bclParser.open("bclParserTest.bcl");
        bool hasNext = bclParser.has_next();
        REQUIRE(!hasNext);
    }
    // remove the file after every SECTION
    remove("bclParserTest.bcl");
}
