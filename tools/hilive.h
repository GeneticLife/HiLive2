#ifndef HILIVE_HILIVE_H

#define HILIVE_HILIVE_H

#include "../lib/headers.h"
#include "../lib/definitions.h"
#include "../lib/global_variables.h"
#include "../lib/kindex.h"
#include "../lib/alnstream.h"
#include "../lib/alnout.h"
#include "../lib/parallel.h"
#include "../lib/argument_parser.h"
#include "../lib/tools_static.h"

Task writeNextTaskToBam ( std::deque<AlnOut> & alnouts );
void worker (TaskQueue & tasks, TaskQueue & finished, TaskQueue & failed, std::deque<AlnOut> & alnouts, std::atomic<CountType> & writing_threads, bool & surrender );
#ifdef TEST
int testableMain(int argc, const char* argv[]);
#else
int main(int argc, const char* argv[]);
#endif

#endif
